<?php

declare(strict_types=1);

namespace MageMastery\Todo\Test\Unit\Service;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Magento\Framework\Api\SearchCriteria;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use MageMastery\Todo\Service\TaskRepository;
use MageMastery\Todo\Model\ResourceModel\Task;
use MageMastery\Todo\Model\ResourceModel\Task\Collection;
use MageMastery\Todo\Model\ResourceModel\Task\CollectionFactory as TaskCollectionFactory;
use MageMastery\Todo\Model\TaskFactory;
use MageMastery\Todo\Api\Data\TaskSearchResultInterface;
use MageMastery\Todo\Api\Data\TaskSearchResultInterfaceFactory;


class TaskRepositoryTest extends TestCase
{
    /**
     * @var MockObject
     */
    private $searchCriteria;

    /**
     * @var MockObject
     */
    private $taskCollectionFactory;

    /**
     * @var MockObject
     */
    private $resource;
    private $taskFactory;
    private $collectionProcessor;
    private $taskSearchResultFactory;

    private $taskRepository;


    protected function setUp()
    {
        $this->searchCriteria = $this->getMockBuilder(SearchCriteria::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->taskCollectionFactory = $this->createMock(TaskCollectionFactory::class);

        $this->resource = $this->getMockBuilder(Task::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->taskFactory = $this->getMockBuilder(TaskFactory::class)
            ->disableOriginalConstructor()
            ->setMethods(['create'])
            ->getMock();

        $this->collectionProcessor = $this->getMockForAbstractClass(
            CollectionProcessorInterface::class,
            [],
            '',
            false,
            false,
            true,
            ['process']
        );

        $this->taskSearchResultFactory = $this->getMockForAbstractClass(
            TaskSearchResultInterfaceFactory::class,
            [],
            '',
            false,
            false,
            true,
            ['create']
        );

        $this->taskRepository = new TaskRepository(
            $this->resource,
            $this->taskFactory,
            $this->collectionProcessor,
            $this->taskSearchResultFactory
        );
    }

    public function testGetTask()
    {
        $taskId = 1;
        $task = $this->taskRepository->getTask($taskId);

        $this->assertEquals($taskId, $task->getTaskId());

        $this->assertEquals($this->isInstanceOf(\MageMastery\Todo\Model\Task::class), $this->isInstanceOf($task));
    }

    public function testGetList()
    {
        $collection = $this->createMock(Collection::class);

        $this->taskCollectionFactory->expects($this->once())
            ->method('create')
            ->willReturn($collection);

        $searchResults = $this->createMock(TaskSearchResultInterface::class);
        $this->taskSearchResultFactory->expects($this->once())
            ->method('create')
            ->willReturn($searchResults);

        $collection->expects($this->once())
            ->method('getItems')
            ->willReturn([]);
        $searchResults->expects($this->once())
            ->method('setItems')
            ->willReturnSelf();

        $tasks = $this->taskRepository->getList($this->searchCriteria);

        $this->assertEquals($searchResults, $tasks);
    }
}